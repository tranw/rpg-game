public class Armour {

  public int acBonus;
  public String name;

  public Armour(String n) {
    name = n;
  }

  public Armour(String n, int ac) {
    name = n;
    acBonus = ac;
  }
}
