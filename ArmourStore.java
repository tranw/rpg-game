import java.util.*;
import java.lang.*;

public class ArmourStore {
  
  public Armour[] inven = new Armour[5];
  public Random rand = new Random();

  public ArmourStore() {
    inven[0] = randItem(rand.nextInt(5));
    inven[1] = randItem(rand.nextInt(5));
    inven[2] = randItem(rand.nextInt(5));
    inven[3] = randItem(rand.nextInt(5));
    inven[4] = randItem(rand.nextInt(5));
  }

  public Armour randItem(int x) {
    Armour thing = null;

    if(x == 0 || x == 1 ){
      thing = new LightArmour("leather");
    }else if (x == 2 || x == 3) {
      thing = new LightArmour("studded leather");
    }else if (x == 4) {

      x = rand.nextInt(6);
      if(x < 3){
        return randItem(x);
      }else {
        if(x < 5){
          thing = new MedArmour("chain mail");
        }else {
          thing = new HeavyArmour("plate mail");
        }
      }
    }
    return thing;
}

  public void purchase(Character pc, int index) {
    Armour curr0 = pc.currArmour;
    Armour curr1 = this.inven[index];
    int len = this.inven.length;
    int c = 0;

    if(curr1.name.equals("leather")){
      c = 15;
    }else if (curr1.name.equals("studded leather")) {
      c = 20;
    }else if(curr1.name.equals("chain mail")) {
      c = 30;
    }else{
      c = 50;
    }

    if(pc.coin < c){
      System.out.println("You do not have enought coin for that.");
      this.displayInven();
      return;
    }else {
      pc.coin -= c;
    }
    Armour thing = null;
    if(curr1.name.contains("leather")){
      thing = new LightArmour(curr1.name);
    }else if(curr1.name.contains("chain")){
      thing = new MedArmour(curr1.name);
    }else{
      thing = new HeavyArmour(curr1.name);
    }
    pc.changeArmour(thing);
    this.inven[index] = null;
    this.displayInven();
  }

  public void displayInven() {
    int len = inven.length;
    for(int i = 0; i < len; i++){
      if(inven[i] != null){
        Armour curr = inven[i];
        System.out.println(i + ". " + curr.name);

      }
    }
  }
}
