import java.util.*;
import java.lang.*;

public class Character {

  Random rand = new Random();
  public String name;
  public int str;
  public int dex;
  public int ac;
  public int level;
  public Item[] inventory;
  public int Currhp;
  public int Maxhp;
  public Armour currArmour;
  public Weapon main;
  public boolean dead;
  public int coin;
  public boolean eB;
  public boolean[] hands;
  public int strBonus;

  public Character(String type) {

    inventory = new Item[10];
    hands = new boolean[2];

    name = type;
    hands[0] = false;
    hands[1] = false;

    if (type.equals("goblin")) {
      Currhp = 5;
      Maxhp = 5;
      level = 1;
      str = 12;
      dex = 10;
      coin = 2;
      currArmour = new LightArmour("leather");
      ac = 10 + (dex - 10)/2 + currArmour.acBonus;
      main = new Weapon("short sword", 4, 1, 0);

    }else if (type.equals("orc")) {
      Currhp = 8;
      Maxhp = 8;
      level = 1;
      str = 14;
      dex = 10;
      coin = 5;
      currArmour = new LightArmour("leather");
      ac = 10 + (dex - 10)/2 + currArmour.acBonus;
      main = new Weapon("falchion", 10, 1, 0);
    }else if (type.equals("gargole")) {
      Currhp = 12;
      Maxhp = 12;
      level = 2;
      str = 16;
      dex = 12;
      coin = 10;
      currArmour = new Armour("nat", 4);
      ac = 10 + (dex - 10)/2 + currArmour.acBonus;
      main = new Weapon("bite", 12, 2, 0);
    }else if ((type.equals("troll"))) {
      Currhp = 20;
      Maxhp = 20;
      level = 4;
      str = 18;
      dex = 18;
      coin = 13;
      currArmour = new Armour("nat", 4);
      ac = 10 + (dex - 10)/2 + currArmour.acBonus;
      main = new Weapon("bite", 12, 2, 0);
    }

    hands[0] = true;
  }

  public void displayInven() {
    int len = inventory.length;
    for(int i = 0; i < len; i++){
      if(inventory[i] != null){
        Item curr = inventory[i];
        System.out.println(curr.name + "      " + curr.amount);

      }
    }

    System.out.println("\ncurrent equiped weapon\n" + main.name);
    System.out.println("\ncurrent equiped armour\n" + currArmour.name);
    System.out.println("\ncurrent coinage \n" + coin + " gold");

  }
  public void displayStats() {
    System.out.println("Current hp " + Currhp + "/" + Maxhp);
    System.out.println("Strength " + str);
    System.out.println("Dexterity " + dex);
    System.out.println("Armour class " + ac);
  }

  public void checkDead() {
    if(this.Currhp <= 0){
      dead = true;
    }
  }

  public void changeHp(int dam) {
    this.Currhp = this.Currhp - dam;
    if(this.Currhp > this.Maxhp){
      this.Currhp = this.Maxhp;
    }
    this.checkDead();
  }

  public int dealDam(Character perve) {
    int toHit = rand.nextInt(20) + 1 + this.level + this.main.bonus;
    int dam = 0;
    if(toHit >= perve.ac){
      dam = rand.nextInt(this.main.maxDamage) + 1 + (this.str - 10)/2 + this.main.bonus;
    }
    return dam;

  }

  public void getloot(Combat deadGuys) {
    int len = deadGuys.enemies.length;
    Character[] dudes = deadGuys.enemies;
    int loot = 0;
    for(int i = 0; i < len; i++){
        loot += dudes[i].coin;
    }
    System.out.println("You have gained " + loot + " coins from looting the corpses");
    this.coin += loot;
  }

  public void rest() {
    if(this.Currhp < this.Maxhp){
      this.Currhp += this.level;
      if(this.Currhp > this.Maxhp){
        this.Currhp = this.Maxhp;
      }
      System.out.println("Current Hp is " + this.Currhp);
    }else {
      System.out.println("You are at max hp.");
    }
  }

  public void levelUp() {
    System.out.println("You leveled up you dumb fuck!");
    this.Maxhp += this.level*2;
    this.str += this.level;
    this.dex += this.level;
    this.level++;
    this.calcAC();
  }

  public void calcAC() {
    this.ac = (this.dex - 10)/2 + 10 + currArmour.acBonus;
  }

  public void eat() {
    int len = this.inventory.length;
    Item curr = null;

    for(int i = 0; i < len; i++){
      curr = this.inventory[i];

      if(curr.name.equals("food")){
        if(curr.amount > 0){
          this.inventory[i].amount--;
          i = len;
        }else {
          this.Maxhp--;
          i = len;
        }
      }
    }
  }
  public void eatBonus() {
    if(this.eB == true){
      System.out.println("You have already gotten the food bonus.");
      return;
    }else {
    System.out.println("You eat some food.");
      this.eat();
      this.eB = true;
      this.str += 2;
    }
  }

  public int findIndex(String thing) {
    int len = this.inventory.length;
    Item curr = null;

    for(int i = 0; i < len; i++){
      curr = this.inventory[i];
      if(curr == null){
        return len;
      }
      if(curr.name.equals(thing)){
        return i;
      }
    }
    return len;
  }

  public void changeWeapon(Weapon change) {
    int currH = this.main.hands;

    if((currH == 2) && (change.hands == 2)){
      this.main = change;
    }else if ((currH == 1) && (change.hands == 2)){
      if(this.hands[1] == false){
        this.main = change;
        this.hands[1] = true;
      }else{
        System.out.println("You do not have enough hands to carry that");
      }
    }else {
      this.main = change;
    }
  }

  public void usePotion(String thing) {
    int index = findIndex(thing);

    if(index == inventory.length){
      System.out.println("You do not have any of those potions.");
      return;
    }
    Item curr = this.inventory[index];
    if(curr.name.equals("healing potion")){
      this.changeHp(-(rand.nextInt(8)+2));
    }else {
      this.strBonus += rand.nextInt(4)+2;
      this.str += this.strBonus;
    }
    this.inventory[index].amount -= 1;
    if(this.inventory[index].amount == 0){
      this.inventory[index] = null;
    }

  }

  public boolean useRope(int difMod) {
    int index = findIndex("rope");
    int amonNeed = 10 + 2*difMod;

    Item curr = this.inventory[index];

    if(curr.amount < amonNeed){
      return false;
    }else {
      this.inventory[index].amount -= amonNeed;
      return true;
    }
  }

  public void changeArmour(Armour thing) {
    this.currArmour = thing;
    this.calcAC();
  }
}
