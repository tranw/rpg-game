import java.util.*;

public class Combat {

  Character[] enemies;
  public boolean done;
  String[] init;
  Random rand;

  public Combat(String type) {

    rand = new Random();

    if(type.equals("intro")){
      enemies = new Character[2];
      enemies[0] = new Character("goblin");
      enemies[1] = new Character("goblin");
    }else if (type.equals("first")){
      enemies = new Character[3];
      enemies[0] = new Character("goblin");
      enemies[1] = new Character("goblin");
      enemies[2] = new Character("goblin");
    }else if (type.equals("second")){
      enemies = new Character[1];
      enemies[0] = new Character("gargole");
    }else if (type.equals("third")) {
      enemies = new Character[3];
      enemies[0] = new Character("orc");
      enemies[1] = new Character("orc");
      enemies[2] = new Character("orc");
    }else if (type.equals("fourth")) {
      enemies = new Character[1];
      enemies[0] = new Character("troll");
    }
  }

  public void checkDone() {
    int len = enemies.length;
    boolean last = true;
    for(int i = 0; i < len; i++){
      last = last && enemies[i].dead;
      if((i == len-1) && last){
        done = true;
      }
    }
  }

}
