import java.util.*;
import java.lang.*;

public class GameUtil{

  public static Scanner kbs = new Scanner(System.in);
  public static Random rand = new Random();

  public static void crossCrevas(int disMod) {
    boolean across = false;

    while(!across){
      System.out.println("\nThe crevas is roughly " +  (5 + disMod*5) + " feet across\n");
      System.out.println("\nWould you like to use some rope or jump the crevas.\n");
      String input = kbs.next();

      if(input.equals("jump")){
        int x = rand.nextInt(20) + 1 + (Game.pc.dex - 10)/2;

        if (x >= 10 + (5*disMod)) {
          System.out.println("You managed to jump across.");
          across = true;
        }else if (rand.nextInt(20) + 1 + (Game.pc.str -10)/2 >= 17) {
          System.out.println("You fall to your death.");
          System.exit(0);
        }else {
          System.out.println("You didn't make it across but you managed to pull yourself out");
        }
      }else {
        across = Game.pc.useRope(disMod);
        if(across == false){
          System.out.println("You do not have enough rope.");
        }
      }
    }

  }

  public static void combatCode(String type) {
    Combat intro = new Combat(type);
    int i = 0;
    Character CurrEn = null;
    String input = "shit";
    int dam = 0;
    int j = 0;
    CurrEn = intro.enemies[i];
    int len = intro.enemies.length;
    Character attacker = null;

    while(!intro.done){
      if(CurrEn.dead == true){
        i++;
        j++;
        CurrEn = intro.enemies[i];
      }
      input = kbs.next();

      while((input.equals("stats")) || (input.equals("inventory"))){

        if(input.equals("inventory")){
          Game.pc.displayInven();
        }else if (input.equals("stats")) {
          Game.pc.displayStats();
        }
        input = kbs.next();
      }
      if(input.equals("hit")){
        dam = Game.pc.dealDam(CurrEn);
        if(dam > 0){
          CurrEn.changeHp(dam);
          System.out.println("You hit and dealt " + dam + " damage.");
        }else {
          System.out.println("you missed");
        }
      }else if (input.equals("retreat")) {
        System.out.println("Don't run away get back in there.");
      }else if (input.equals("eat")) {
        Game.pc.eatBonus();
      }else if (input.equals("potion")) {
        System.out.println("health or strength?");
        input = kbs.next();
        if(input.equals("health")){
          Game.pc.usePotion("health potion");
        }else{
          Game.pc.usePotion("strength potion");
        }
      }

      for(int k = j; k < len; k++){
        attacker = intro.enemies[k];
        dam = attacker.dealDam(Game.pc);

        if(dam > 0){
          Game.pc.changeHp(dam);
          System.out.println("You've been hit for " + dam + " damage.");
        }else {
          System.out.println("they missed");
        }
      }
      if(Game.pc.dead == true){
        System.out.println("you're dead sucka");
        System.exit(0);
      }
      System.out.println();
      intro.checkDone();
    }

    Game.pc.getloot(intro);

  }

  public static void storecode() {
    String input = "shit";
    Store thing = new Store();
    thing.displayInven();
    System.out.println("To purchase something type the number coresponding to the item");

    while(!input.equals("continue")){
      input = kbs.next();
      if(input.equals("inventory")){
        Game.pc.displayInven();
      }else if (input.equals("stats")) {
        Game.pc.displayStats();
      }else{
        try{
          int index = Integer.parseInt(input);
          if(index > -1 && index < 5){
            thing.purchase(Game.pc, index);
          }
        }catch(Exception e){
          continue;
        }
      }
    }
  }

  public static void armourstorecode() {
    String input = "shit";
    ArmourStore thing = new ArmourStore();
    thing.displayInven();
    System.out.println("To purchase something type the number coresponding to the item");

    while(!input.equals("continue")){
      input = kbs.next();
      if(input.equals("inventory")){
        Game.pc.displayInven();
      }else if (input.equals("stats")) {
        Game.pc.displayStats();
      }else{
        try{
          int index = Integer.parseInt(input);
          if(index > -1 && index < 5){
            thing.purchase(Game.pc, index);
          }
        }catch(Exception e){
          continue;
        }
      }
    }
  }


  public static void weaponStorecode() {
    String input = "shit";
    WeaponStore thing = new WeaponStore();
    thing.displayInven();
    System.out.println("To purchase something type the number coresponding to the item");

    while(!input.equals("continue")){
      input = kbs.next();
      if(input.equals("inventory")){
        Game.pc.displayInven();
      }else if (input.equals("stats")) {
        Game.pc.displayStats();
      }else{
        try{
          int index = Integer.parseInt(input);
          if(index > -1 && index < 5){
            thing.purchase(Game.pc, index);
          }
        }catch(Exception e){
          continue;
        }
      }
    }
  }

  public static void postCombat() {
    String input = "fuck";
    if(Game.pc.eB == true){
      Game.pc.eB = false;
      Game.pc.str -= 2;
    }
    if(Game.pc.strBonus != 0){
      Game.pc.str -= Game.pc.strBonus;
      Game.pc.strBonus = 0;
    }
    while(!input.equals("continue")){
      input = kbs.next();
      if(input.equals("inventory")){
        Game.pc.displayInven();
      }else if (input.equals("stats")) {
        Game.pc.displayStats();
      }else if (input.equals("rest")) {
        Game.pc.rest();
      }
    }
  }

  public static void cong() {
    System.out.println("\nCongrats you win\n");
  }
}
