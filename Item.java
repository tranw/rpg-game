public class Item {

  public String name;
  public int amount;

  public Item(String n, int howMuch) {
    name = n;
    amount = howMuch;
  }
}
