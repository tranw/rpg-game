public class LightArmour extends Armour {

  public LightArmour(String name) {
    super(name);
    if(name.equals("leather")){
      acBonus = 2;
    }else if(name.equals("studded leather")){
      acBonus = 3;
    }
  }
}
