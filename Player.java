public class Player extends Character {

  public Player(String type) {
    super(type);
    Currhp = 15;
    Maxhp = 15;
    level = 1;
    str = 30;
    dex = 16;
    coin = 15;
    currArmour = new LightArmour("studded leather");
    ac = 10 + (dex - 10)/2 + currArmour.acBonus;
    main = new Weapon("sword", 6, 1, 0);
    inventory[0] = new Item("rope", 50);
    inventory[1] = new Item("cooking stuff", 1);
    inventory[2] = new Item("food", 5);

  }

}
