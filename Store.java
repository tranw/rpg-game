import java.util.*;

public class Store {

  public Item[] inven;
  public Random rand;

  public Store() {

    inven = new Item[5];
    rand = new Random();

    inven[0] = randItem(rand.nextInt(5));
    inven[1] = randItem(rand.nextInt(5));
    inven[2] = randItem(rand.nextInt(5));
    inven[3] = randItem(rand.nextInt(5));
    inven[4] = randItem(rand.nextInt(5));
  }

  public Item randItem(int x) {
    Item thing = null;

    if(x == 0 || x == 1 ){
      thing = new Item("food", 5);
    }else if (x == 2 || x == 3) {
      thing = new Item("rope", 10);
    }else if (x == 4) {
      x = rand.nextInt(2);
      if(x == 0){
        thing = new Item("healing potion", 1);
      }else{
        thing = new Item("strength potion", 1);
      }
    }

    return thing;
  }

  public void purchase(Character pc, int index) {
    Item[] inventory = pc.inventory;
    Item curr0 = null;
    Item curr1 = this.inven[index];
    int len = inventory.length;
    int c = 0;

    if(curr1.name.equals("food")){
      c = curr1.amount*2;
    }else if (curr1.name.equals("rope")) {
      c = curr1.amount;
    }else {
      c = 50;
    }

    if(pc.coin < c){
      System.out.println("You do not have enought coin for that.");
      this.displayInven();
      return;
    }else {
      pc.coin -= c;
    }

    for(int i = 0; i < len; i++){
      curr0 = inventory[i];
      if((curr0 != null) && (curr0.name.equals(curr1.name))){
        curr0.amount += curr1.amount;
        this.inven[index] = null;
        this.displayInven();
        return;
      }
    }
    for(int i = 0; i < len; i++){

      if(pc.inventory[i] == null){
        pc.inventory[i] = new Item(curr1.name, curr1.amount);
        this.inven[index] = null;
        this.displayInven();
        return;
      }
    }

    System.out.println("You do not have space for that Item");



  }

  public void displayInven() {
    int len = inven.length;
    for(int i = 0; i < len; i++){
      if(inven[i] != null){
        Item curr = inven[i];
        System.out.println(i + ". " + curr.name + "      " + curr.amount);

      }
    }
  }
}
