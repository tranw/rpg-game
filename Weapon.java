public class Weapon {

  public String name;
  public int maxDamage;
  public int hands;
  public int bonus;

  public Weapon(String n, int dam, int h, int b) {
    name = n;
    maxDamage = dam;
    hands = h;
    bonus = b;
  }
}
