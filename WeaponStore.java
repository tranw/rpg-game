import java.util.*;
import java.lang.*;

public class WeaponStore {
  public Weapon[] inven;
  public Random rand;

  public WeaponStore() {

    inven = new Weapon[5];
    rand = new Random();

    inven[0] = randItem(rand.nextInt(5));
    inven[1] = randItem(rand.nextInt(5));
    inven[2] = randItem(rand.nextInt(5));
    inven[3] = randItem(rand.nextInt(5));
    inven[4] = randItem(rand.nextInt(5));
  }

  public Weapon randItem(int x) {
    Weapon thing = null;

    if(x == 0 || x == 1 ){
      thing = new Weapon("short sword", 4, 1, 0);
    }else if (x == 2 || x == 3) {
      thing = new Weapon("sword", 6, 1, 0);
    }else if (x == 4) {

      x = rand.nextInt(6);
      if(x < 3){
        return randItem(x);
      }else {
        if(x < 5){
          thing = new Weapon("long sword", 8, 1, 0);
        }else {
          thing = new Weapon("great sword", 12, 1, 0);
        }
      }
    }
    return thing;
}

  public void purchase(Character pc, int index) {
    Weapon curr0 = pc.main;
    Weapon curr1 = this.inven[index];
    int len = this.inven.length;
    int c = 0;

    if(curr1.name.equals("short sword")){
      c = 15;
    }else if (curr1.name.equals("sword")) {
      c = 20;
    }else if(curr1.name.equals("long sword")) {
      c = 30;
    }else{
      c = 45;
    }

    if(pc.coin < c){
      System.out.println("You do not have enought coin for that.");
      this.displayInven();
      return;
    }else {
      pc.coin -= c;
    }
    Weapon thing = new Weapon(curr1.name, curr1.maxDamage, curr1.hands, curr1.bonus);
    pc.changeWeapon(thing);
    this.inven[index] = null;
    this.displayInven();
  }

  public void displayInven() {
    int len = inven.length;
    for(int i = 0; i < len; i++){
      if(inven[i] != null){
        Weapon curr = inven[i];
        System.out.println(i + ". " + curr.name);

      }
    }
  }
}
